<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// ======================================FRONTEND=======================================

//Index
Route::get('/web','Frontend\HomeController@index');

Route::group([
    'prefix' => 'member',
    'namespace' => 'Frontend',
], function () {
	Route::get('/login','MemberController@indexLoign')->name('frontend.member.login');
	Route::post('/signin','MemberController@signin');
	Route::get('/register','MemberController@indexRegister');
	Route::post('/insert-register','MemberController@register');
	Route::get('/logout', 'MemberController@logout');
	Route::post('/update-member', 'MemberController@updateMember');
});

Route::middleware('member')->group(function(){
	//Blog - Rate - Comment
	Route::get('/blog','Frontend\BlogController@index');
	Route::get('/blogSingle/{id}','Frontend\BlogController@blogSingle');
	Route::post('/insert-rate','Frontend\BlogController@insertRate');
	Route::post('/add-comment/{id}','Frontend\BlogController@insertComment');

	//Account
	Route::get("/account", "Frontend\MemberController@account");
});

//Cart
Route::get('/cart','Frontend\CartController@showCart');
Route::post('/insert-cart','Frontend\CartController@insert');
Route::post('/update-qty-up','Frontend\CartController@updateQtyUp');
Route::post('/update-qty-down','Frontend\CartController@updateQtyDown');
Route::post('/delete-cart','Frontend\CartController@delete');
Route::get('/checkout','Frontend\CartController@checkout');
Route::post('/register-checkout','Frontend\CartController@registerCheckout');

//Checkout - Mail - Order
Route::post('/order','Frontend\MailController@order');

//Product - Search
Route::get("myproduct", "Frontend\ProductController@index");
Route::get("add-product", "Frontend\ProductController@add");
Route::post('insert-product', 'Frontend\ProductController@insertProduct');
Route::get('edit-product/{id}', 'Frontend\ProductController@edit');
Route::post('update-product/{id}', 'Frontend\ProductController@updateProduct');
Route::get('delete-product/{id}', 'Frontend\ProductController@delete');
Route::get('details/{id}', 'Frontend\ProductController@productDetails');
Route::post('/search', 'Frontend\ProductController@search');
Route::get('/search-advanced', 'Frontend\ProductController@searchAdvanced');
Route::post('/search-advanced', 'Frontend\ProductController@searchAdvanced');

Route::post('/search-ajax', 'Frontend\ProductController@searchAjax');

Route::get('/category/{id}', 'Frontend\ProductController@productCategory');
Route::get('/brand/{id}', 'Frontend\ProductController@productBrand');


// ======================================ADMIN=======================================
Auth::routes();

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', 'HomeController@index')->name('home');

Route::group([
 	'prefix'     => 'admin', //tien to vao sau link
    'namespace'  => 'Admin',
    'middleware' => ['admin']
], function () {
	//Index
	Route::get('/dashboard','DashboardController@index')->name('dashboard');

	//Country
	Route::get('/country','CountryController@index');
	Route::get('/add-country','CountryController@add');
	Route::post('/insert-country','CountryController@insert');
	Route::get('/delete-country/{id}','CountryController@destroy');

	//Profile
	Route::get('/profile','UserController@index');
	Route::post('/update-profile', 'UserController@update');

	//Blog
	Route::get('/blog','BlogController@index');
	Route::get('/add-blog','BlogController@add');
	Route::post('/insert-blog','BlogController@insert');
	Route::get('/edit-blog/{id}','BlogController@edit');
	Route::post('/update-blog/{id}','BlogController@update');
	Route::get('/delete-blog/{id}','BlogController@destroy');

	//Category
	Route::get('/category', 'CategoryController@index');
	Route::get('add-category', 'CategoryController@add');
	Route::post('insert-category', 'CategoryController@insert');
	Route::get('edit-category/{id}', 'CategoryController@edit');
	Route::post('update-category/{id}', 'CategoryController@update');
	Route::get('/delete-category/{id}','CategoryController@delete');

	//Brand
	Route::get('/brand', 'BrandController@index');
	Route::get('/add-brand', 'BrandController@add');
	Route::post('/insert-brand', 'BrandController@insert');
	Route::get('edit-brand/{id}', 'BrandController@edit');
	Route::post('update-brand/{id}', 'BrandController@update');
	Route::get('/delete-brand/{id}','BrandController@delete');

	//Product
	Route::get('product', 'ProductController@index');
	Route::get('add-product', 'ProductController@add');
	Route::get('edit-product', 'ProductController@edit');

});
