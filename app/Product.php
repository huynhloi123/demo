<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'product';
    protected $primaryKey = 'id';
    protected $fillable = ['name', 'price', 'user_id', 'category_id', 'brand_id', 'type', 'sale_price', 'company', 'images', 'description'];
}
