<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rate extends Model
{
	protected $table = 'rate';
    protected $primaryKey = 'id';
    protected $fillable = ['blog_id', 'user_id', 'qty'];
}
