<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
   	protected $table = 'blog';
	protected $primaryKey = 'id';
    protected $fillable = ['user_id', 'title', 'image', 'description', 'content'];

    public function user() {
        return $this->belongsTo('App\User', 'id');
    }

    public function comment() {
        return $this->hasMany('App\Comment');
    }
}
