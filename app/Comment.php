<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table = 'comment';
    protected $primaryKey = 'id';
    protected $fillable = ['blog_id', 'user_id', 'name', 'avatar', 'content', 'level'];

    public function blog() {
        return $this->belongsTo('App\Blog', 'id');
    }
}
