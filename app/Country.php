<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
	protected $table = 'country';
	protected $primaryKey = 'id';
    protected $fillable = ['country_name'];

   	public function user() {
		return $this->hasMany('App\User');
	}
}
