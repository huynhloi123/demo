<?php

namespace App\Http\Middleware;
use Auth;
use Closure;

class AdminAuthentication
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if( Auth::check() && Auth::user()->level == 1 ){
            return $next($request);
        }else{
            Auth::logout();
            return redirect('/login');
        }
    }
}
