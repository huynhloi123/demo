<?php

namespace App\Http\Middleware;
use Auth;
use Closure;

class AuthenticationMember
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if( Auth::check() && Auth::user()->level == 0 ){  
            return $next($request);
            
        }else{
            Auth::logout();
            return redirect()->route('frontend.member.login')->with('error','You are not a member ! please register new account.');
        }
    }
}
