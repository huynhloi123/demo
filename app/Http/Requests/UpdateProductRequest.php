<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'price' => 'required|numeric',
            'category_id' => 'required',
            'brand_id' => 'required',
            'type' => 'required',
            'sale_price' => 'numeric',
            'company' => 'required',
            'imgSave.*'  =>  'required|mimes:jpeg,png,jpg,gif,svg',
            //|max:10
            'desc' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'required' => ':attribute must be not empty',
            'numeric' => ':attribute phải là số',
            'image' => ':atrribute chỉ được up ảnh',
            'mimes' => 'chi dc up anh ',
            //'max' => 'ko dc qua :max size',
        ];
    }
}
