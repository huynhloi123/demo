<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:20',
            'email' => 'required|email',
            'phone' => 'required|numeric',
            'address' => 'required',
            'avatar' => 'image|max:2048',
            'country_id' => 'required|not_in:0',
        ];
    }

    public function messages()
    {
        return [
            'required' => ':attribute must be not empty',
            'email' => ':attribute không đúng định dạng email',
            'max' => ':attribute không được quá :max ký tự',
            'numeric' => ':attribute phải là số',
            'image' => ':atrribute chỉ được up ảnh'
        ];
    }
}
