<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BlogRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|min:3',
            'image' => 'image',
            'description' => 'required',
            'content' => 'required|min:20'
        ];
    }

    public function messages()
    {
        return [
            'required' => ':attribute must be not empty',
            'min' => ':attribute phải nhiều hơn :min ký tự',
            'image' => ':atrribute chỉ được up ảnh'
        ];
    }
}
