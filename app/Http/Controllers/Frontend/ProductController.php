<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\ProductRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Category;
use App\Brand;
use App\Product;
use Auth;
use Image;
use DB;
class ProductController extends Controller
{
    public function index() {
        $getData = Product::where('user_id', Auth::id())->get()->toArray();
    	return view('frontend.product.product')->with(compact('getData'));
    }

    public function add() {
    	$category = Category::all();
    	$brand    = Brand::all();
    	return view('frontend.product.addProduct')->with(compact('category', 'brand'));
    }
    
   	public function insertProduct(ProductRequest $request) {
        if($request->hasfile('imgSave')){
           $array = $this->saveImage($request->file('imgSave'));
           $size  = $this->getSizeImage($request->file('imgSave'));
        }
        //dd($array);
        $countImage = count($array);
        $sumSize    = array_sum($size);
    
        if ($countImage <= 3 && $sumSize <= 1024000) {
            $images = json_encode($array);
            $request->merge(['user_id' => Auth::id(), 'images' => $images]);
            if (Product::create($request->all())) {
                    return redirect('/myproduct')->with('success', __('Save product successfully.'));
            } else {
                return back()->with('error', 'Your product Save NOT successfully');
            }

        } else {
            return back()->with('error', 'Only maximum 3 images can be uploaded');
        }
    }

    public function edit($id) {
        $category = Category::all();
        $brand    = Brand::all();
        $productByIdUser = Product::find($id)->toArray();
        return view('frontend.product.editProduct')->with(compact('productByIdUser','category', 'brand'));
    }

    public function updateProduct(UpdateProductRequest $request, $id) {
        $data = $request->all();
        //Dùng để update nên không dùng ->toArray
        $getData = Product::find($id);
        $getImagesById = Product::find($id)->toArray();
        //Lấy ảnh trong DB => json_decode thành mảng    
        $imgDB = json_decode($getImagesById['images']);

        //Lấy ảnh cần xóa
        if ($request->imgDelete) {
            $imgDelete = $data['imgDelete'];
        } else {
            $imgDelete = [];
        }

        //Người dùng chọn ảnh để update
        if($request->hasfile('imgSave')){
            $array = $this->saveImage($request->file('imgSave'));
            //So sánh 2 mảng và trả về những giá trị khác nhau, Lấy ảnh còn lại
            $compare  = array_diff($imgDB, $imgDelete);
            //Gộp 2 mảng: hình ảnh còn lại và hình ảnh người dùng thêm
            $sumImage = array_merge($compare, $array);
            //Chuyển $sumImage thành mảng liên tục để để mảng bắt đầu lại từ [0] và xóa không bị lỗi
            $resetKey = array_values($sumImage);
            $data['images'] = json_encode($resetKey);
            $countImage     = count($sumImage);

            if ($countImage <= 3) {
                if ($getData->update($data)) {
                    $this->deleteImage($imgDelete);
                    return redirect('/myproduct')->with('success', __('Update product successfully.'));
                } else {
                    return back()->with('error', 'Your product deleted NOT successfully');
                }

            } else {
                return back()->with('error', 'Can not upload more than 3 photos');
            }

        //Không chọn ảnh
        } else {
            //Lấy dl cột images trong DB và so sánh với ảnh cần xóa => ảnh còn lại của DB 
            $compare  = array_diff($imgDB, $imgDelete);
            //Chuyển $compare thành mảng liên tục để mảng bắt đầu lại từ [0]
            $resetKey = array_values($compare);
            $data['images'] = json_encode($resetKey);
            $countImage     = count($compare);

            //Số lượng ảnh dưới 1 => Số lượng image trong DB = Số lượng image cần xóa => mảng $compare rỗng
            if ($countImage < 1) {
                return back()->with('error', 'Product must has at least 1 photo');
            } else {
                if ($getData->update($data)) {
                    $this->deleteImage($imgDelete);
                    return redirect('/myproduct')->with('success', __('Update product successfully.'));
                } else {
                    return back()->with('error', 'Your product deleted NOT successfully');
                }
            }
        }
    }

    protected function deleteImage($imgs) {
        //dd($imgs);
        try {
            if (isset($imgs) && !empty($imgs)) {
                foreach ($imgs as $image) {
                    $deleteImage  = public_path('upload/product/' . $image);
                    $deleteImage1 = public_path('upload/product/' . "img50-" . $image);
                    $deleteImage2 = public_path('upload/product/' . "img200-" . $image);
                    if (isset($imgs) && !empty($deleteImage)) {
                        unlink($deleteImage);
                    }
                    if (isset($imgs) && !empty($deleteImage1)) {
                        unlink($deleteImage1);
                    }
                    if (isset($imgs) && !empty($deleteImage2)) {
                        unlink($deleteImage2);
                    }
                }
            }
        } catch (\Exception $e) {
            return redirect('/myproduct')->with('error', __('Your product deleted NOT successfully'));
        }
        
    }

    protected function getSizeImage($fileImage) {
        //dd($fileImage);
        $sizeImage  = [];
        foreach($fileImage as $image){
            $size   = $image->getSize();
            $size_2 = "img50-".$image->getSize();
            $size_3 = "img200-".$image->getSize();
            $sizeImage[] = $size;
        }
        //dd($sizeImage);
        return $sizeImage;
    }

    protected function saveImage($fileImage) {
        //dd($fileImage);
        $array = [];
        foreach($fileImage as $image){

            $name   = strtotime(date('Y-m-d H:i:s')) . '_' . $image->getClientOriginalName();
            $name_2 = "img50-" . $name;
            $name_3 = "img200-" . $name;

            //$image->move('upload/product/', $name);
            
            $path  = public_path('upload/product/' . $name);
            $path2 = public_path('upload/product/' . $name_2);
            $path3 = public_path('upload/product/' . $name_3);

            Image::make($image->getRealPath())->save($path);
            Image::make($image->getRealPath())->resize(50, 70)->save($path2);
            Image::make($image->getRealPath())->resize(200, 300)->save($path3);
            
            $array[] = $name;         
        }
        //dd($array);
        return $array;
    }

    public function delete($id) {
        $productById = Product::find($id);
        $deleteImage = json_decode($productById->images);
        try {
            if ($productById->delete()) {
                $this->deleteImage($deleteImage);
                return back()->with('success', 'Your product deleted successfully');
            }
        } catch (\Exception $e) {
            return back()->with('error', 'Your product deleted NOT successfully');
        }

    }
    
    public function productDetails($id) {
        $category = Category::all();
        $brand    = Brand::all();
        $productDetails = Product::find($id);
        $getImg   = json_decode($productDetails->images);
        return view('frontend.product.productDetails')->with(compact('productDetails', 'getImg', 'category', 'brand'));
    }

    public function productCategory($id) {
        $category = Category::all();
        $brand    = Brand::all();
        $name     = Category::select('category_name')->where('id', $id)->get()->toArray();
        $product  = Product::where('category_id', $id)->get()->toArray();
        return view('frontend.product.productCategory')->with(compact('product', 'category', 'brand', 'name'));
    }

    public function productBrand($id) {
        $category = Category::all();
        $brand    = Brand::all();
        $name     = Brand::select('brand_name')->where('id', $id)->get()->toArray();
        $product  = Product::where('brand_id', $id)->get()->toArray();
        return view('frontend.product.productBrand')->with(compact('product', 'category', 'brand', 'name'));
    }

    public function search(Request $request) {
        $data     = $request->all();
        $category = Category::all();
        $brand    = Brand::all();
        $keyWord  = $data['keyWord'];
        $product  = Product::where('name', 'like', '%'.$keyWord.'%')
        ->orwhere('price', 'like', '%'.$keyWord.'%')
        ->orwhere('sale_price', 'like', '%'.$keyWord.'%')->get()->toArray();
        return view('frontend.product.searchProduct')->with(compact('product', 'keyWord', 'category', 'brand'));
    }

    public function searchAdvanced(Request $request) {
        $category = Category::all();
        $brand    = Brand::all();
        $data     = DB::table('product');
        //$data   = Product::where('id', '<>', 0);
        $getPrice = $request->price;
        //chuyển chuỗi thành mảng để dung whereBetween
        $getPrice = explode('-', $getPrice);
        if (!empty($request->name)) {
            $data->where('name', 'like', '%'.$request->name.'%');
        }

        if (!empty($request->price)) {
            $data->whereBetween('price', $getPrice);
        }

        if (!empty($request->category_id)) {
            $data->where('category_id', $request->category_id);
        }

        if (!empty($request->brand_id)) {
            $data->where('brand_id', $request->brand_id);
        }

        if (!empty($request->type)) {
            $data->where('type', $request->type);
        }

        //Nếu người dùng không chọn thì hiển thị sp dưới dạng phân trang
        if (empty($request->name) && empty($request->price) && empty($request->category_id) && empty($request->brand_id) && empty($request->type)) {
            $product = Product::paginate(3);
            return view('frontend.product.searchAllProduct')->with(compact('product', 'category', 'brand'));
        } else {
            $product = $data->get();
            return view('frontend.product.searchAdvancedProduct')->with(compact('product', 'category', 'brand'));
        }
    }

    public function searchAjax(Request $request) {
        $price = json_decode(stripslashes($_POST['jsonPrice']));
        foreach ($price as $key => $value) {
            $array[] = $value;
        }

        $product = Product::whereBetween('price', $array)->get()->toArray();

        // echo "<pre>";
        // print_r($product);
        // echo "</pre>";
return response()->json([
    'product' => $product
]);
    
       
    }
}

