<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\RegisterCheckoutRequest;
use App\Product;
use App\User;
use Session;
use Auth;
use Hash;
session_start();
class CartController extends Controller
{
    public function insert(Request $request) {
    	$data = $request->all();
    	if (session::has('cart') && !empty(session('cart'))) {
			$cart = Session::get('cart');
		} else {
			$cart = [];
		}
		$checkCart = true;

		if ($cart) {
			foreach ($cart as $key => $value) {
				if ($key == $data['id']) {
					$cart[$key]['qty'] +=  1;
					$checkCart = false;
				} 
			}
		} 

		if ($checkCart) {
			$product = array(
				'id'    => $data['id'],
				'name'  => $data['name'],
				'image' => $data['image'],
				'price' => $data['price'],
				'qty'   => $data['qty']
			);
			$cart[$data['id']] = $product;
		}

		Session::put('cart', $cart);
		$qty = count(Session('cart'));
		return $qty;
		//Session::flush();   
    }

    public function showCart() {
    	return view('frontend.cart.cart');
    }

    public function updateQtyUp(Request $request) {
    	$data  = $request->all();
    	$id_up = $data['id_up'];
    	if (session::has('cart') && !empty(session('cart'))) {
	    	$cart  = Session::get('cart');
			$up = [];
			$up['subTotal'] = 0;
			foreach ($cart as $key => $list) {
				$up['subTotal'] += ($list['qty'] * $list['price']);
				if ($key == $id_up) {
					$cart[$id_up]['qty'] = $cart[$id_up]['qty'] + 1;
					$up['qty'] = $cart[$id_up]['qty'];
					$up['totalPrice'] = number_format(($cart[$id_up]['price'] * $cart[$id_up]['qty']));
					$up['subTotal']  += $cart[$id_up]['price'];				
				}
			}
		}	
		Session::put('cart', $cart);
		return $up;
    }

    public function updateQtyDown(Request $request) {
    	$data    = $request->all();
    	$id_down = $data['id_down'];
    	if (session::has('cart') && !empty(session('cart'))) {
	    	$cart = Session::get('cart');
			$down = [];
			$down['subTotal'] = 0;
			foreach ($cart as $key => $list) {
				$down['subTotal'] += ($list['qty'] * $list['price']);
				if ($key == $id_down) {
					$cart[$id_down]['qty'] = $cart[$id_down]['qty'] - 1;
					$down['qty'] = $cart[$id_down]['qty'];
					$down['totalPrice'] = number_format(($cart[$id_down]['price'] * $cart[$id_down]['qty']));
					$down['subTotal']  -= $cart[$id_down]['price'];				
				}

				if ($list['qty'] == 0 && $id_down == $list['id']) {
					unset($cart[$key]);
					Session::put('cart', $cart);
					$down['qtySession'] = count(Session('cart'));
				}
			}
		}
		Session::put('cart', $cart);
		return $down;
    }

    public function delete(Request $request) {
    	$data = $request->all();
    	$id_delete = $data['id_delete'];
    	if (session::has('cart') && !empty(session('cart'))) {
    		$cart = Session::get('cart');
	    	foreach ($cart as $key => $list) {
				if ($id_delete == $list['id']) {
					unset($cart[$key]);
					Session::put('cart', $cart);
					$qty = count(Session('cart'));
					return $qty;
				}
			}
    	}
    }

   	public function checkout() {
    	return view('frontend.cart.checkout');
    }

    public function registerCheckout(RegisterCheckoutRequest $request) {
        $password = Hash::make($request->password);
        $level    = 0;
        $request->merge(['password' => $password, 'level' => $level]);
        try {        	
        	if (User::create($request->all())) {
	           return redirect('/member/login')->with('success', 'Acount Register Successfully');
	        }
        } catch (\Exception $e) {
        	return back()->with('error', 'Acount Register NOT Successfully');
        }     
    }
}
