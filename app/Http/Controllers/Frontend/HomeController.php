<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Category;
use App\Brand;
use App\Product;
class HomeController extends Controller
{
    public function index() {
    	$category = Category::all();
        $brand    = Brand::all();
    	$product  = Product::orderBy('created_at', 'desc')->limit(6)->get();
        return view('frontend.index')->with(compact('product', 'category', 'brand'));
    }

}
