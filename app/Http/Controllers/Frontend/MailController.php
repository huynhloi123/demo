<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\RegisterCheckoutRequest;
use App\History;
use App\User;
use Mail;
use Auth;
use Session;
class MailController extends Controller
{
    public function order(Request $request) {
        $user = User::find(Auth::id());
        if (session()->has('cart') && session('cart') != NULL) {
            $cart = Session::get('cart');
            //$cart = session('cart');
            //$cart = Session('cart');
            $subTotal = 0;
            foreach ($cart as $key => $value) { 
                $subTotal += ($value['price'] * $value['qty']);
            }

            //dl để đưa sang frontend
            $data = [
                'cart'     => $cart,
                'user'     => $user,
                'subTotal' => $subTotal
            ];

            try {
                Mail::send('frontend.mail.mail', $data, function($mail) use ($user, $subTotal) {
                    $mail->from('huynhloi0910@gmail.com', 'loihuynh');
                    $mail->to($user->email, $user->name);
                    $mail->subject('Thu gui dat hang thanh cong');
                    
                    History::create([
                    'user_id' => $user->id,
                    'name'    => $user->name,
                    'email'   => $user->email,
                    'phone'   => $user->phone,
                    'price'   => $subTotal
                    ]);
                });

                Session::put('cart', NULL);
                return back()->with('success', 'Order successfully, please check mail to view your order information');
                
            } catch (\Exception $e) {
                return back()->with('error', 'Order Not successfully, please to try again');
            }
            
        } else {
            return redirect('/checkout')->with('error', 'You don\'t have any orders to pay ! Please Shopping now');
        }
    }
}
