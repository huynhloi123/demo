<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\RegisterMemberRequest;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\UpdateProfileRequest;
use Illuminate\Support\Facades\Hash;
use App\Country;
use App\User;
use Auth;
use Session;
session_start();
class MemberController extends Controller
{
    public function __construct(){
        //Check login, trừ add_category
        $this->middleware('member' , ['except' => ['indexLoign', 'signin', 'indexRegister', 'register']]);
    }
	
	public function indexRegister() {
    	$country = Country::all();
        return view('frontend.member.register')->with(compact('country'));
    }

    // =============Register==============
    public function register(RegisterMemberRequest $request) {

        $data = $request->all();
        $file = $request->avatar;
        try {
            if(!empty($file)){
                $ext = '.'.$file->getClientOriginalExtension();
                $fileName = str_replace($ext, date('d-m-Y-H-i-s') . $ext, $file->getClientOriginalName());
                $data['avatar'] = $fileName;
            }

            $data['password'] = Hash::make($request->password);
            $data['level']    = 0;
            if (User::create($data)) {
                $file->move('upload/user/avatar', $fileName);
                return redirect('member/login')->with('success', 'Acount Register Successfully');
            }
        } catch (\Exception $e) {
            return redirect('member/register')->with('error', 'Acount Register NOT Successfully');
        }

    }


    // =============Login==============
    public function indexLoign() {
        if (Auth::check()) {
           return redirect('/web');
        } else {
            return view('frontend.member.login');
        }    
    }

    public function signin(LoginRequest $request) {
    	// $data = $request->all();
    	// $email = $request->email;
    	// $password = md5($request->password);
    	
    	// $user = new User();
        //first = limit(1) ,lay cai dau tien
    	// $result = User::where('email', $email)->where('password', $password)->first();

    	// if ($result) {
    	// 	return redirect('web')->with('success', 'Login Successfully');
    	// } else {
    	// 	return redirect('web/login')->with('success', 'Login Fails');
    	// }

		$login = [
		            'email' => $request->email,
		            'password' => $request->password,
		            'level' => 0
		        ];

        $remember = false;

        if ($request->remember_me) {
            $remember = true;
        }   
		        
        // = where slect
        if (Auth::attempt($login, $remember)) {
            return redirect('web')->with('success', 'Login Successfully');
        }else {
            return redirect('member/login')->with('error', 'Login Fails');
        }
	}

    // =============Logout==============
    public function logout() {
        if (Auth::check()) {
            Auth::logout();
            Session::flush();
            return redirect('member/login');
        }    
    }

    // ============Account=============
    public function account() {
        $user    = Auth::user();
        $country = Country::all();
        return view('frontend.member.account')->with(compact('user', 'country'));
    }

    // ============Update Member=============
    public function updateMember(UpdateProfileRequest $request) {
        $userId = Auth::id();
        $data   = $request->all();
        $file   = $request->avatar;
        $memberById = User::find($userId);

        if ($memberById->avatar) {
            $pathImage = 'upload/user/avatar/'.$memberById->avatar;
        }

        try {
            if (!empty($file)) {
                $ext = '.'.$file->getClientOriginalExtension();
                $fileName = str_replace($ext, date('d-m-Y-H-i-s').$ext, $file->getClientOriginalName());
                $data['avatar'] = $fileName;
            }

            if ($data['password']) {
                $data['password'] = bcrypt($data['password']);
            } else {
                $data['password'] = $memberById->password;
            }

            if ($memberById->update($data)) {
                if(!empty($file)){
                    if (isset($pathImage) && !empty($pathImage)) {
                        unlink($pathImage);
                    }
                    $file->move('upload/user/avatar', $fileName);
                }
                return redirect('member/login')->with('success', __('Update profile success.'));
            }
        } catch (\Exception $e) {
            return redirect('account')->withErrors('Update profile error.');
        }


    }
}
