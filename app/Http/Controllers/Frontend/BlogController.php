<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\CommentRequest;
use Auth;
use App\Blog;
use App\Comment;
use App\Rate;
use App\User;
use App\Category;
use App\Brand;
class BlogController extends Controller
{
    public function index() {
        $category = Category::all();
        $brand    = Brand::all();
    	$blog     = Blog::orderBy('created_at', 'desc')->paginate(3);
        $nameUser = User::select('name')->where('id', Auth::id())->get()->toArray();
        return view('frontend.blog.blogList')->with(compact('blog' , 'nameUser', 'category', 'brand'));
    }

    public function blogSingle($id) {
        $category   = Category::all();
        $brand      = Brand::all();
        $blogSingle = Blog::find($id);
        $nameUser   = User::where('id', Auth::id())->get();
        $comment    = Comment::where('blog_id', $id)->get();
        $avgRate    = Rate::where('blog_id', $id)->where('user_id', Auth::id())->avg('qty');
        $avgRate    = round($avgRate);
        return view('frontend.blog.blogSingle')->with(compact('blogSingle', 'nameUser', 'comment' , 'avgRate', 'category', 'brand'));
    }

    public function insertRate(Request $request) {
        $request->merge(['user_id' => Auth::id()]);
        try {
            if (Rate::create($request->all())) {
                echo 'done';
            }   
        } catch (\Exception $e) {
            return redirect('/blogSingle/'.$request->blog_id)->with('error', 'Add Star NOT Successfully');
        }      
    }

    public function insertComment(CommentRequest $request, $id) {
        try {
            $request->merge([
                'blog_id'   => $id, 
                'user_id'   => Auth::id(),
                'name'      => Auth::user()->name,
                'avatar'    => Auth::user()->avatar
            ]);    
            if (Comment::create($request->all())) {
               return redirect('/blogSingle/'.$id)->with('success', 'Add Comment Successfully');
            }
        } catch (\Exception $e) {
            return redirect('/blogSingle/'.$id)->with('error', 'Add Comment NOT Successfully');
        }  
    }
}



    // $getBlogDetail = Blog::with(['comment' => function ($q) {
    //           $q->orderBy('id', 'desc');
    //         }])->find($id);
