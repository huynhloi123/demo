<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\BlogRequest;
use App\Blog;
use Auth;
class BlogController extends Controller
{
    public function index() {
        $blog = Blog::paginate(4);
        return view('admin.blog.blog')->with(compact('blog'));
    }

    public function add() {
        return view('admin.blog.addBlog');
    }

    public function insert(BlogRequest $request) {   
        $data = $request->all();
        $file = $request->image;

        try {
            if(!empty($file)){
                $ext = '.'.$file->getClientOriginalExtension();
                $fileName = str_replace($ext, date('d-m-Y-H-i-s') . $ext, $file->getClientOriginalName());
                $data['image'] = $fileName;
            }
            
            $data['user_id'] = Auth::id();
            if(Blog::create($data)){
                if(!empty($file)){
                    $file->move('upload/blog/images', $fileName);
                }
                return redirect('admin/blog')->with('success', 'Data inserted Successfully');
            }          
        } catch (\Exception $e) {
            return redirect('admin/blog')->with('error', 'Data inserted NOT Successfully');
        } 
    }

    public function edit($id) {
        $blogEdit = Blog::find($id);
        return view('admin.blog.editBlog')->with(compact('blogEdit'));
    }

    public function update(BlogRequest $request, $id) {
        $data = $request->all();
        $file = $request->image;
        //$file = request('image');
        $blog = Blog::find($id);
        $pathImage = 'upload/blog/images/'.$blog->image;

        try {
            if(!empty($file)){
            $ext = '.'.$file->getClientOriginalExtension();
            //tìm kiếm đuôi .jpg và thay = xxx.jgp = abcxxx.jpg
            $fileName = str_replace($ext, date('d-m-Y-H-i-s') . $ext, $file->getClientOriginalName());
            $data['image'] = $fileName;

            } else {
                $data['image'] = $blog->image;
            }

            if ($blog->update($data)) {
                if (!empty($file)) {
                    $file->move('upload/blog/images', $fileName);
                    if (isset($pathImage) && !empty($pathImage)) {
                        unlink($pathImage);
                    }
                }
                return redirect('admin/blog')->with('success', 'Data Updated Successfully');
            }
        } catch (\Exception $e) {
            return redirect('admin/blog')->with('error', 'Data Updated NOT Successfully');
        }           
    }

    public function destroy($id) {   
        $blog = Blog::find($id);
        $file = $blog->image;
        if ($file) {
            $pathImage = 'upload/blog/images/'.$file;
        } else {
            $pathImage = '';
        }
        
        try {
            if ($blog->delete()) {
                if (isset($pathImage) && !empty($pathImage)) {
                    unlink($pathImage);
                } 
                return redirect('admin/blog')->with('success', 'Data Deleted Successfully');
            }
        } catch (\Exception $e) {
            return redirect('admin/blog')->with('error', 'Data Deleted NOT Successfully');
        }
    }
}
