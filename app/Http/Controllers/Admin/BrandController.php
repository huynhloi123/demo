<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\BrandRequest;
use App\Brand;
class BrandController extends Controller
{
    public function index() {
    	$brand = Brand::all();
    	return view('admin.product.brand.brand')->with(compact('brand'));
    }

    public function add() {
    	return view('admin.product.brand.addBrand');
    }

    public function insert(BrandRequest $request) {
        try {
            if (Brand::create($request->all())) {
                return redirect('/admin/brand')->with('success', 'Data Inserted Successfully');
            } 
        } catch (\Exception $e) {
            return redirect()->back()->with('error', 'Data Inserted Not Successfully');
        }
    }

    public function edit($id) {
    	$brandById = Brand::find($id);
    	return view('admin.product.brand.editBrand')->with(compact('brandById'));
    }

    public function update(BrandRequest $request, $id) {
        //update data loại trừ _token ra
        $request->offsetUnset('_token');
        try {
            if (Brand::find($id)->update($request->all())) {
                return redirect('/admin/brand')->with('success', 'Data Updated Successfully');   
            } 
        } catch (\Exception $e) {
            return redirect()->back()->with('error', 'Data Updated Not Successfully');
        } 	
    }

    public function delete($id) {
    	$brandById = Brand::find($id);
        try {
            if ($brandById->delete()) {
                return redirect('/admin/brand')->with('success', 'Data Deleted Successfully');  
            }
        } catch (\Exception $e) {
            return redirect()->back()->with('error', 'Data Deleted Not Successfully');
        }
    }
}
