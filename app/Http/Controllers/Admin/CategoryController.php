<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\CategoryRequest;
use App\Category;
class CategoryController extends Controller
{
    public function index() {
    	$category = Category::all();
    	return view('admin.product.category.category')->with(compact('category'));
    }

    public function add() {
    	return view('admin.product.category.addCategory');
    }

    public function insert(CategoryRequest $request) {
        try {
            if (Category::create($request->all())) {
                return redirect('/admin/category')->with('success', 'Data Inserted Successfully');
            }
        } catch (\Exception $e) {
            return redirect()->back()->with('error', 'Data Inserted Not Successfully');
        }
    }

    public function edit($id) {
    	$categoryById = Category::find($id);
    	return view('admin.product.category.editCategory')->with(compact('categoryById'));
    }

    public function update(CategoryRequest $request, $id) {
        //Chỉ update category_name
        try {
            if (Category::find($id)->update($request->only('category_name'))) {
                return redirect('/admin/category')->with('success', 'Data Updated Successfully');   
            }
        } catch (\Exception $e) {
            return redirect()->back()->with('error', 'Data Updated Not Successfully');
        }
    }

    public function delete($id) {
    	$categoryById = Category::find($id);
        try {
            if ($categoryById->delete()) {
                return redirect('/admin/category')->with('success', 'Data Deleted Successfully');   
            }
        } catch (\Exception $e) {
            return redirect()->back()->with('error', 'Data Deleted Not Successfully');
        }
    }
}
