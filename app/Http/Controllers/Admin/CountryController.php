<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\CountryRequest;
use App\Country;
class CountryController extends Controller
{
    public function index() {
        $country = Country::all();
        return view('admin.country.country')->with(compact('country'));
    }

    public function add() {
        return view('admin.country.addCountry');
    }

    public function insert(CountryRequest $request) {
        try {
            if (Country::create($request->all())) {
                return redirect('admin/country')->with('success', 'Data Inserted Successfully');
            } 
        } catch (\Exception $e) {
            return redirect('admin/country')->with('error', 'Data Inserted Not Successfully');
        }
    }

    public function destroy($id) {
        try {
            if (Country::find($id)->delete()) {
                return redirect('/admin/country')->with('success', 'Data Deleted Successfully');
            }
        } catch (\Exception $e) {
            return redirect('admin/country')->with('error', 'Data Deleted Not Successfully');
        }    
    }
}
