<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\UpdateProfileRequest;
use Auth;
use App\User;
use App\Country;
class UserController extends Controller
{
    public function index() {
        $data    = Auth::user();
        $country = Country::all();
        return view('admin.user.profile')->with(compact('data', 'country'));
    }

    public function update(UpdateProfileRequest $request) {
        $userId = Auth::id();
        $user   = User::findOrFail($userId);
        $data   = $request->all();
        $file   = $request->avatar;
        if ($user->avatar) {
            $pathImage = 'upload/user/avatar/'.$user->avatar;
        }

        try {
            if(!empty($file)){
                $ext = '.'.$file->getClientOriginalExtension();
                $fileName = str_replace($ext, date('d-m-Y-H-i-s') . $ext, $file->getClientOriginalName());
                $data['avatar'] =  $fileName;
            }
        
            if ($data['password']) {
                $data['password'] = bcrypt($data['password']);
            }else{
                $data['password'] = $user->password;
            }
           
            if ($user->update($data)) {
                if(!empty($file)){
                    if (isset($pathImage) && !empty($pathImage)) {
                        unlink($pathImage);
                    }
                    $file->move('upload/user/avatar', $fileName);
                }
                return redirect('admin/profile')->with('success', __('Update profile success.'));
            }
        } catch (\Exception $e) {
            return redirect('admin/profile')->withErrors('Update profile error.');
        } 
    }
}
