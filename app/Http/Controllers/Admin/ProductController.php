<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Category;
use App\Brand;
class ProductController extends Controller
{
    public function index() {
   		return view('admin.product.product.product');
   	}

    public function add() {
    	$category = Category::all();
    	$brand 	  = Brand::all();
   		return view('admin.product.product.addProduct')->with(compact('category', 'brand'));
   	}

   	public function edit() {
   		$category = Category::all();
    	$brand 	  = Brand::all();
   		return view('admin.product.product.editProduct')->with(compact('category', 'brand'));
   	}
}
