@extends('admin.master.master')
@section('title')
    Country
@endsection
@section('content')
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Country</h4>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex align-items-center justify-content-end">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="#">Home</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Dashboard</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <div class="row">
                @if(session('success'))
                <div class="alert alert-success">
                    {{session('success')}}
                </div>
                @endif

                @if(session('error'))
                <div class="alert alert-danger">
                    {{session('error')}}
                </div>
                @endif
                    <div class="col-12">
                        <div class="card">
                        

                            <div class="table-responsive">
                                <table class="table">
                                    <thead class="thead-light">
                                        <tr>
                                            <th scope="col">No.</th>
                                            <th scope="col">Name Country</th>
                                            <th scope="col">Action</th>
                                        </tr>

                                    </thead>
                                    <tbody>
                                        @foreach($country as $countries)
                                        <tr>
                                            <th scope="row">{{ $loop->index + 1}}</th>
                                            
                                            <td>{{$countries->country_name}}</td>
                                            <td>
                                                <a onclick = "return confirm('Are you want to delete???')" href="{{ URL::to('admin/delete-country/'.$countries->id) }}"><i class="mdi mdi-account-remove"></i>Delete</a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>

                                </table>
                            </div>
                        </div>
                        <a  class="btn btn-success"  href="{{ url('/admin/add-country') }}">Add Country</a>
                         
                        
                    </div>
                </div>
            </div>

        </div>

@endsection