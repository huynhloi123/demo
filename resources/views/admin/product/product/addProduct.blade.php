@extends('admin.master.master')
@section('title')
    Add Product
@endsection
@section('content')
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Add Product</h4>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex align-items-center justify-content-end">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="#">Home</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Dashboard</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <div class="row">
                    <div class="card-body">
                        <form method="POST" class="form-horizontal form-material" action="{{  URL::to('/admin/insert-brand') }}">
                            @csrf
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                <div class="form-group">
                                    <label class="col-md-12">Product Name</label>
                                    <div class="col-md-12">
                                        <input type="text" name="name" placeholder="Enter name Product..." class="form-control form-control-line">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-12">Price</label>
                                    <div class="col-md-12">
                                        <input type="text" name="price" placeholder="Enter name Product..." class="form-control form-control-line">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-12">Select Category</label>
                                    <div class="col-sm-12">
                                        <select class="form-control form-control-line" name="category_id" >
                                            <option value="0">-----Choose Category-----</option>            
                                            @foreach($category as $categories)
                                                <option value="{{ $categories->id }}">{{$categories->category_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-12">Select Brand</label>
                                    <div class="col-sm-12">
                                        <select class="form-control form-control-line" name="brand_id" required>
                                            <option value="0">-----Choose Brand-----</option>      
                                                @foreach($brand as $brands)
                                                    <option value="{{ $brands->id }}">{{$brands->brand_name}}</option>
                                                @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-12">Select Type</label>
                                    <div class="col-sm-12">
                                        <select class="form-control form-control-line" id="type" name="type" required>
                                            <option value="0" class="new">New</option>
                                            <option value="1" class="sale">Sale</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-12">
                                        <input type="text" name="sale_price" placeholder="Sale Price" value="0" style="display: none;" class="salePrice"/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-12">Company</label>
                                    <div class="col-md-12">
                                        <input type="text" name="company" placeholder="Enter name Company..." class="form-control form-control-line">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-12">Image</label>
                                    <div class="col-md-12">
                                        <input type="file" name="filename[]" class="form-control form-control-line">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-12">Details</label>
                                    <div class="col-md-12">
                                        <input type="text" name="desc" placeholder="Enter Details..." class="form-control form-control-line">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <button type="submit" class="btn btn-success">Add</button>
                                    </div>
                                </div>
                        </form>
                    </div>
                    </div>
                </div>
            </div>

                </div>
            </div>
        </div>

        
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script type="text/javascript">
            $(document).ready(function(){
                $("#type").change(function() {
                    var New = $(this).find('.new').attr('value');
                    var sale = $(this).find('.sale').attr('value');
                    if (sale == 1) {
                        $(".salePrice").toggle();
                    } 
                    // console.log(New);
                    // console.log(sale);
                });

                $("#file").change(function(){
                    var error = '';
                    var files = $('#file')[0].files;

                    if (files.length > 3) {
                        error += '<p>Chi duoc upload toi da 3 anh</p>';
                    } else if (files.length == '') {
                        error += '<p>Khong duoc bo trong anh</p>';
                    } else if (files.size > 1024) {
                        error += '<p>File anh khong duoc qua 1MB</p>';
                    }

                    if (error != '') {
                        //$('#file').val('');
                        $('#error_file').html('<span class="text-danger">'+error+'</span>');
                        return false;
                    } 
                });
            });
    </script>

@endsection