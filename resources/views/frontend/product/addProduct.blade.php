@extends('frontend.master.master')
@section('title')
	Add Product | E-Shopper
@endsection
@section('content')
    <div class="container">
        <div class="row">
        	<div class="col-sm-9 padding-right">
				<h2 class="title text-center">Add Product</h2>	
				<div class="login-form"><!--login form-->
					
					<form method="post" action="{{url('insert-product')}}" enctype="multipart/form-data">
						@csrf
						@if ($errors->any())
			                <div class="alert alert-danger">
			                    <ul>
			                        @foreach($errors->all() as $error)
			                        <li>{{ $error }}</li>
			                        @endforeach
			                    </ul>
			                </div>
			            @endif

			            @if(session('success'))
			            <div class="alert alert-success">
			                {{session('success')}}
			            </div>
			            @endif

			            @if(session('error'))
			            <div class="alert alert-danger">
			                {{session('error')}}
			            </div>
			            @endif
			            <input type="text" name="name" placeholder="Name" value="">
						<input type="text" name="price" placeholder="Price" value="" />
						<select name="category_id" style="margin-bottom: 10px; padding: 10px">
							<option value="0">-----Choose Category-----</option>
							@foreach($category as $categories)
								<option value="{{$categories->id}}">{{$categories->category_name}}</option>
							@endforeach
						</select>
						<select name="brand_id" style="margin-bottom: 10px; padding: 10px">
							<option value="0">-----Choose Brand-----</option>
							@foreach($brand as $brands)
								<option value="{{$brands->id}}">{{$brands->brand_name}}</option>
							@endforeach
						</select>
						<select id="type" name="type" style="margin-bottom: 10px; padding: 10px">
								<option value="1" class="new">New</option>
								<option value="2" class="sale">Sale%</option>
						</select>
						<input type="text" name="sale_price" placeholder="Sale Price" value="0" style="display: none;" class="salePrice"/>
						<input type="text" name="company" placeholder="Company" value="" />						
						<input type="file" id="file" name="imgSave[]" accept="image/*" multiple>
						<span id="error_file"></span>
						<textarea name="description" rows="5" style="resize: none" placeholder="Details"></textarea>
						<button type="submit" class="btn btn-default">Add Product</button>
					</form>
				</div><!--/login form-->
			</div>
        </div>
    </div>

    <script type="text/javascript">
    $(document).ready(function(){
    	$("#type").change(function() {
    		var New = $(this).find('.new').attr('value');
    		var sale = $(this).find('.sale').attr('value');
    		if (sale == 2) {
    			$(".salePrice").toggle();
    		} 
    		// console.log(New);
    		// console.log(sale);
    	});

    	$("#file").change(function(){
    		var error = '';
    		var files = $('#file')[0].files;

    		if (files.length > 3) {
    			error += '<p>Chi duoc upload toi da 3 anh</p>';
    		} else if (files.length == '') {
    			error += '<p>Khong duoc bo trong anh</p>';
    		} else if (files.size > 1024) {
    			error += '<p>File anh khong duoc qua 1MB</p>';
    		}

    		if (error != '') {
    			//$('#file').val('');
    			$('#error_file').html('<span class="text-danger">'+error+'</span>');
    			return false;
    		} 
    	});
    });
	</script>

@endsection