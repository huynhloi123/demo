@extends('frontend.master.master')
@section('title')
	Edit Product | E-Shopper
@endsection
@section('content')
    <div class="container">
        <div class="row">
        	<div class="col-sm-9 padding-right">
				<h2 class="title text-center">Edit Product</h2>	
				<div class="login-form"><!--login form-->
					
					<form method="post" action="{{url('update-product/'.$productByIdUser['id'])}}" enctype="multipart/form-data">
						@csrf
						@if ($errors->any())
			                <div class="alert alert-danger">
			                    <ul>
			                        @foreach($errors->all() as $error)
			                        <li>{{ $error }}</li>
			                        @endforeach
			                    </ul>
			                </div>
			            @endif

			            @if(session('success'))
			            <div class="alert alert-success">
			                {{session('success')}}
			            </div>
			            @endif


			            @if(session('error'))
			            <div class="alert alert-danger">
			                {{session('error')}}
			            </div>
			            @endif

			            @php
							$img = json_decode($productByIdUser['images']);
							//print_r($img[0]);
						@endphp

			            <input type="text" name="name" placeholder="Name" value="{{$productByIdUser['name']}}">
						<input type="text" name="price" placeholder="Price" value="{{$productByIdUser['price']}}" />

						<select name="category_id" style="margin-bottom: 10px; padding: 10px">
							<option value="0">-----Choose Category-----</option>
							@foreach($category as $categories)
								@if($categories->id == $productByIdUser['category_id'])
									<option selected value="{{$categories->id}}">{{$categories->category_name}}</option>
									@else
										<option value="{{$categories->id}}">{{$categories->category_name}}</option>
								@endif
							@endforeach
						</select>

						<select name="brand_id" style="margin-bottom: 10px; padding: 10px">
							<option value="0">-----Choose Brand-----</option>
							@foreach($brand as $brands)
								@if($brands->id == $productByIdUser['brand_id'])
									<option selected value="{{$brands->id}}">{{$brands->brand_name}}</option>
									@else
										<option value="{{$brands->id}}">{{$brands->brand_name}}</option>
								@endif
							@endforeach
						</select>

						<select id="type" name="type" style="margin-bottom: 10px; padding: 10px">
							@if($productByIdUser['type'] == 1)
								<option selected value="1" class="new">New</option>
								<option value="2" class="sale">Sale%</option>
								@else
								<option value="1" class="new">New</option>
								<option selected value="2" class="sale">Sale%</option>
							@endif
						</select>

						@if($productByIdUser['type'] == 1)
						<input type="text" name="sale_price" placeholder="Sale Price" value="0" style="display: none;" class="salePrice"/>
							@else 
							<input type="text" name="sale_price" placeholder="Sale Price" value="{{$productByIdUser['sale_price']}}" class="salePrice"/>
						@endif

						<input type="text" name="company" placeholder="Company" value="{{$productByIdUser['company']}}" />	

					
						<input type="file" id="file" name="imgSave[]" multiple class="form-control ">
						<span id="error_file"></span>
						
						@foreach($img as $imgs)
						<div class="col-sm-2">
						<img src="{{asset('upload/product/'.$imgs)}}" style="height: 100px; image-rendering: pixelated; display: inline-block;">
							<span style="color: red">click checkbox to delete</span>
							<input type="checkbox" id="images" name="imgDelete[]" value="{{$imgs}}">			
						</div>
						@endforeach
					
						<textarea name="desc" rows="5" style="resize: none" placeholder="Details">{{$productByIdUser['description']}}</textarea>
						<button type="submit" class="btn btn-default">Update</button>
					</form>
				</div><!--/login form-->
			</div>
        </div>
    </div>

    <script type="text/javascript">
    $(document).ready(function(){
    	$("#type").change(function() {
    		var New = $(this).find('.new').attr('value');
    		var sale = $(this).find('.sale').attr('value');
    		if (sale == 2) {
    			$(".salePrice").toggle();
    		} 
    		// console.log(New);
    		// console.log(sale);
    	});

    	$("#file").change(function(){
    		var error = '';
    		var files = $('#file')[0].files;

    		if (files.length > 3) {
    			error += '<p>Chi duoc upload toi da 3 anh</p>';
    		} else if (files.length == '') {
    			error += '<p>Khong duoc bo trong anh</p>';
    		} else if (files.size > 1024) {
    			error += '<p>File anh khong duoc qua 1MB</p>';
    		}

    		if (error != '') {
    			//$('#file').val('');
    			$('#error_file').html('<span class="text-danger">'+error+'</span>');
    			return false;
    		} 
    	});
    });
	</script>

@endsection