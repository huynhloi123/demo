@extends('frontend.master.master')
@section('title')
	Product Category | E-Shopper
@endsection
@section('content')

<section>
	<div class="container">
		<div class="row">
			
			<div class="col-sm-9 padding-right">
				<div class="features_items"><!--features_items-->
					@foreach($name as $names)
					<h2 class="title text-center">{{$names['category_name']}}</h2>
					@endforeach
					<?php
						if (empty($product)) {
							echo '<span style="color:red; font-size:20px;">Currently, There are no products in this category</span>';
						}
					?>
					@foreach($product as $products)
					<?php 
						$img = json_decode($products['images']);
						//print_r($img);
					?>
					@if($products['type'] == 1)
					<div class="col-sm-4">
						<div class="product-image-wrapper">
							<div class="single-products">
								<div class="productinfo text-center">
									<img src="{{asset('upload/product/'.$img[0])}}" style="width: 220px; height: 170px; image-rendering: pixelated" alt="" />
									<h2 id="price">{{number_format($products['price'])}}</h2>
									<p id="name">{{$products['name']}}</p>
									<button type="button" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</button>
								</div>
								<div class="product-overlay">
									<div class="overlay-content">
										<h2>{{number_format($products['price'])}}</h2>
										<p>{{$products['name']}}</p>
									<form>
										<input type="hidden" class="name" value="{{$products['name']}}">
										<input type="hidden" class="image" value="{{$img[0]}}">
										<input type="hidden" class="price" value="{{$products['price']}}">
										<input type="hidden" class="qty" value="1">
										<button type="button" id="{{$products['id']}}}}" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</button>
									</form>
									</div>
								</div>
								<img src="{{asset('frontend/images/home/new.png')}}" class="new" alt="" />
							</div>
							<div class="choose">
								<ul class="nav nav-pills nav-justified">
									<li><a href="#"><i class="fa fa-plus-square"></i>Add to wishlist</a></li>
									<li><a href="{{url('/details/'.$products['id'])}}"><i class="fa fa-plus-square"></i>Product Details</a></li>
								</ul>
							</div>
						</div>
					</div>
					
					@else
					
					<div class="col-sm-4">
						<div class="product-image-wrapper">
							<div class="single-products">
								<div class="productinfo text-center">
									<img src="{{asset('upload/product/'.$img[0])}}" style="width: 220px; height: 170px; image-rendering: pixelated" alt="" />
									<h2>{{number_format($products['price'] * ((100 - $products['sale_price'])/100))}}</h2>				
									{{-- <del style="color: red;">{{number_format($products->price)}}</del> --}}
									<p>{{$products['name']}}</p>
									<button type="button" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</button>
								</div>
								<div class="product-overlay">
									<div class="overlay-content">
										<h2>{{number_format($products['price'] * ((100 - $products['sale_price'])/100))}}</h2>
										<p>{{$products['name']}}</p>
										<form>
											<input type="hidden" class="name" value="{{$products['name']}}">
											<input type="hidden" class="image" value="{{$img[0]}}">
											<input type="hidden" class="price" value="{{$products['price'] * ((100 - $products['sale_price'])/100)}}">
											<input type="hidden" class="qty" value="1">
											<button type="button" id="{{$products['id']}}" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</button>
										</form>
									</div>
								</div>
										<img src="{{asset('frontend/images/home/sale.png')}}" class="new" alt="" />
							</div>
							<div class="choose">
								<ul class="nav nav-pills nav-justified">
									<li><a href="#"><i class="fa fa-plus-square"></i>Add to wishlist</a></li>
									<li><a href="{{url('/details/'.$products['id'])}}"><i class="fa fa-plus-square"></i>Product Details</a></li>
								</ul>
							</div>
						</div>
					</div>
					@endif
					@endforeach	
				</div><!--features_items-->
			</div>
		</div>
	</div>
</section>
<script>
	$(document).ready(function(){
		$('.add-to-cart').click(function() {
			var id = $(this).attr('id');
			var name = $(this).closest('form').find('.name').val();
			var image = $(this).closest('form').find('.image').val();
			var price = $(this).closest('form').find('.price').val();
			var qty = $(this).closest('form').find('.qty').val();

			$.ajax ({
	        	url:"{{url('/insert-cart')}}",
	        	method: "POST",
	        	data: {
	        		"_token": "{{ csrf_token() }}",
	        		id:id,
	        		name:name,
	        		image:image,
	        		price:price, 
	        		qty:qty
	        	},
	        	success:function(data) {
	        		if (data) {
	        			alert('Add product to cart successfully !');
	        		}
	        		$('i.fa-shopping-cart').text(data);
	        	}
	        });

		});
	});
</script>
@endsection
