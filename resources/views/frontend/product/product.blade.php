@extends('frontend.master.master')
@section('title')
	MyProduct | E-Shopper
@endsection
@section('content')

<div class="container">
    <div class="row">
    	<div class="col-sm-9 padding-right">
			<h2 class="title text-center">Product list</h2>	
			<div class="table-responsive cart_info">
				
	            @if(session('success'))
	            <div class="alert alert-success">
	                {{session('success')}}
	            </div>
	            @endif
	            
        		@if(session('error'))
	            <div class="alert alert-danger">
	                {{session('error')}}
	            </div>
	            @endif
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu" style="background: #FE980F; color: #fff;">
							<td style="padding: 15px">STT</td>
							<td class="image">Title</td>
							<td class="image">Image</td>
							<td class="price">Price</td>
							<td class="total">Action</td>	
						</tr>
					</thead>
					<tbody>
						
					@if($getData)
					@php
						$i = 0;
					@endphp
					@foreach($getData as $product)
						<?php 
							$img = json_decode($product['images']);
							$i++;
							//print_r($img);
						?>
						<tr>
							<td><?php echo $i; ?></td>
							<td>{{$product['name']}}</td>
							<td>
								
								<img src="{{asset('upload/product/'.$img[0])}}" style="height: 100px; image-rendering: pixelated">
								
							</td>
							@if($product['type'] == 1)
							<td>{{$product['price']}}</td>
								@else
									<td>{{$product['price'] * ((100 - $product['sale_price'])/100)}}</td>
							@endif
							<td>
								<a href="{{url('edit-product/'.$product['id'])}}"><i class='fa fa-edit'></i></a><br>
								<a href="{{url('delete-product/'.$product['id'])}}"><i class='fa fa-times'></i></a>
							</td>
						</tr>
						
					@endforeach
						@else
							<tr>
							<td colspan="5">
								{{'ban ko co sp nao'}}
							</td>
						</tr>
					@endif

							
					</tbody>
					<tfoot>
		                <tr>
		                    <td colspan="8">
		                      {{--   <button type="submit" class="btn btn-default">Add Product</button> --}}
		                        <a  class="btn btn-warning"  href="{{ url('/add-product') }}">Add Product</a>
		                    </td>
		                </tr>
		            </tfoot>
				</table>
			</div>		
		</div>
    </div>
</div>

@endsection