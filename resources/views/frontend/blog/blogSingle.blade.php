@extends('frontend.master.master')
@section('title')
	Blog Single | E-Shopper
@endsection
@section('content')

		<div class="blog-post-area">
			<h2 class="title text-center">Latest From our Blog</h2>
			<div class="single-blog-post">
				<h3>{{$blogSingle->title}}</h3>
				<div class="post-meta">
					<ul>
						<li>
							<i class="fa fa-user">
								@foreach($nameUser as $name)
									@if($blogSingle->user_id == $name->id)
										{{$name->name}}
										@else 
											{{'KHÁCH'}}
									@endif
								@endforeach
							</i>
						</li>
						<li><i class="fa fa-clock-o"></i> {{$blogSingle->created_at}}</li>
						<li><i class="fa fa-calendar"></i> DEC 5, 2013</li>
					</ul>
            <div class="rate" style="float: right;">
                <div class="vote">
                	@for($i=1; $i <= 5; $i++)
                		@if($i <= $avgRate)
                    <div class="star_{{$i}} ratings_stars {{'ratings_over'}}"><input value="{{$i}}" type="hidden"></div>
                    		@else
                    			  <div class="star_{{$i}} ratings_stars"><input value="{{$i}}" type="hidden"></div>
                    	@endif
                  	@endfor
                    <input class="blog_id" value="{{$blogSingle->id}}" type="hidden">
                    <span class="rate-np">{{$avgRate}}</span>
                </div> 
            </div>
				</div>
				<a href="">
					<img src="{{asset('upload/blog/images/'.$blogSingle->image)}}" style="image-rendering: pixelated" alt="">
				</a>
				<p>{!!$blogSingle->content!!}</p> <br>

				<p>
				<div class="pager-area">
					<ul class="pager pull-right">
						<li><a href="#">Pre</a></li>
						<li><a href="#">Next</a></li>
					</ul>
				</div>
			</div>
		</div><!--/blog-post-area-->

		<div class="rating-area">
			<ul class="ratings">
				<li class="rate-this">Rate this item:</li>
				<li>
					<i class="fa fa-star color"></i>
					<i class="fa fa-star color"></i>
					<i class="fa fa-star color"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
				</li>
				<li class="color">(6 votes)</li>
			</ul>
			<ul class="tag">
				<li>TAG:</li>
				<li><a class="color" href="">Pink <span>/</span></a></li>
				<li><a class="color" href="">T-Shirt <span>/</span></a></li>
				<li><a class="color" href="">Girls</a></li>
			</ul>
		</div><!--/rating-area-->

		<div class="socials-share">
			<a href=""><img src="{{asset('frontend/images/blog/socials.png')}}" alt=""></a>
		</div><!--/socials-share-->

		<div class="media commnets">
			<a class="pull-left" href="#">
				<img class="media-object" src="{{asset('frontend/images/blog/man-one.jpg')}}" alt="">
			</a>
			<div class="media-body">
				<h4 class="media-heading">Annie Davis</h4>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
				<div class="blog-socials">
					<ul>
						<li><a href=""><i class="fa fa-facebook"></i></a></li>
						<li><a href=""><i class="fa fa-twitter"></i></a></li>
						<li><a href=""><i class="fa fa-dribbble"></i></a></li>
						<li><a href=""><i class="fa fa-google-plus"></i></a></li>
					</ul>
					<a class="btn btn-primary" href="">Other Posts</a>
				</div>
			</div>
		</div><!--Comments-->
		<div class="response-area">
			<h2>3 RESPONSES</h2>
			<ul class="media-list">
				@foreach($comment as $comments)
					@if($comments->level == 0)
				<li class="media">					
					<a class="pull-left" href="#">
						<img class="media-object" src="{{asset('upload/user/avatar/'.$comments->avatar)}}" style="height: 100px; image-rendering: pixelated" alt="">
					</a>
					<div class="media-body">
						<ul class="sinlge-post-meta">
							<li><i class="fa fa-user"></i>{{$comments->name}}</li>
							<li><i class="fa fa-clock-o"></i>{{$comments->created_at}}</li>
							<li><i class="fa fa-calendar"></i> DEC 5, 2013</li>
						</ul>
						<p>{{$comments->content}}</p>
						<a class="btn btn-primary" id="{{$comments->id}}" ><i class="fa fa-reply"></i>Replay</a>
					</div>
				</li>
					@endif
				@endforeach

				@foreach($comment as $comments)
					@if($comments->level != 0)
				<li class="media second-media">
					<a class="pull-left" href="#">
						<img class="media-object" src="{{asset('upload/user/avatar/'.$comments->avatar)}}" alt="">
					</a>
					<div class="media-body">
						<ul class="sinlge-post-meta">
							<li><i class="fa fa-user"></i>{{$comments->name}}</li>
							<li><i class="fa fa-clock-o"></i> {{$comments->created_at}}</li>
							<li><i class="fa fa-calendar"></i> DEC 5, 2013</li>
						</ul>
						<p>{{$comments->content}}</p>
					</div>
				</li>
					@endif
				@endforeach
			</ul>					
		</div><!--/Response-area-->
		<div class="replay-box">
			<div class="row">
				<div class="col-sm-4">
					<h2>Leave a replay</h2>
				</div>
				<div class="col-sm-12">

				<form method="post" id="abc" action="{{url('/add-comment/'.$blogSingle->id)}}">
					@csrf
					@if ($errors->any())
		                <div class="alert alert-danger">
		                    <ul>
		                        @foreach($errors->all() as $error)
		                        <li>{{ $error }}</li>
		                        @endforeach
		                    </ul>
		                </div>
		            @endif

		            @if(session('success'))
		            <div class="alert alert-success">
		                {{session('success')}}
		            </div>
		            @endif

		            @if(session('error'))
		            <div class="alert alert-danger">
		                {{session('error')}}
		            </div>
		            @endif
		            
		            <input type="hidden" class="level" name="level" value="0">
					<div class="text-area">
						<div class="blank-arrow">
							<label>Content</label>
						</div>
						<span>*</span>
						<textarea name="content" rows="11"></textarea>
						<button class="btn btn-primary" id="submit_form" type="submit">post comment</button>
					</div>
				</form>
				</div>
			</div>
		</div><!--/Repaly Box-->
	
	<link type="text/css" rel="stylesheet" href="{{asset('frontend/rate/css/rate.css')}}">
    <script src="{{asset('frontend/rate/js/jquery-1.9.1.min.js')}}"></script>

	<script type="text/javascript">
		$('form#abc').submit(function(){
				var loggedIn = "{{Auth::check()}}";
				//var comment_id = $('.comment_id').val();
				//alert(comment_id);
				if ( loggedIn == "" ){ 
		    		alert('Please to login before comment!');
		    		return false;
		    		// console.log('asdasd');
		    	}else {
					return true;
		    	}
		})
	</script>
	<script type="text/javascript">
		$('a.btn-primary').click(function(){
			var getIdComment = $(this).attr('id');
			//var l = JSON.parse(getIdComment)
			var changeLevel = $('.level').attr("value", getIdComment);
			var level = $('.level').attr("value");
			console.log(level);
			return false;
		})
	</script>


    <script>
    	
    	$(document).ready(function(){
			//vote
			$('.ratings_stars').hover(
	            // Handles the mouseover
	            function() {
	                $(this).prevAll().andSelf().addClass('ratings_hover');
	                // $(this).nextAll().removeClass('ratings_vote'); 
	            },
	            function() {
	                $(this).prevAll().andSelf().removeClass('ratings_hover');
	                // set_votes($(this).parent());
	            }
	        );

			$('.ratings_stars').click(function(){
				var qty =  $(this).find("input").val();
				var blog_id  = $(".blog_id").val();
				var loggedIn = "{{Auth::check()}}";


				if ( loggedIn == "" ){ 
		    		alert('Please to login before comment!');
		    		return false;
		    		// console.log('asdasd');
		    	}

		    	if ($(this).hasClass('ratings_over')) {
		            $('.ratings_stars').removeClass('ratings_over');
		            $(this).prevAll().andSelf().addClass('ratings_over');
		        } else {
		        	$(this).prevAll().andSelf().addClass('ratings_over');
		        }

		        $.ajax ({
		        	url:"{{url('/insert-rate')}}",
		        	method: "POST",
		        	data: {
		        		"_token": "{{ csrf_token() }}",
		        		qty:qty, 
		        		blog_id:blog_id
		        	},
		        	success:function(data) {
		        		if (data == 'done') {
		        			alert('Bạn đã đánh giá' + qty + '*');
		        		} else {
		        			alert('lỗi');
		        		}
		        	}
		        });
		    });
	

		});
    </script>
	

@endsection


