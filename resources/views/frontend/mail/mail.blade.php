<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mail</title>
</head>
<body>
    <h2>Hi {{$user->name}}</h2>
    <p>
        <b>You have successfully ordered from our store</b>
    </p>
    <h4>Your order information</h4>
    <h4>Date order: <?php echo date("d/m/Y"); ?></h4>
    <h4>Your order details</h4>
    <h4 style="color: red">Total Price: {{$subTotal}}</h4>
    <table border="1" cellspacing="0" cellpadding="10" width="400">
        <thead>
            <tr class="cart_menu">
                <td class="image">Item</td>
                <td class="description">Name</td>
                <td class="price">Price</td>
                <td class="quantity">Quantity</td>
                <td class="total">Total</td>
            </tr>
        </thead>
        <tbody>
            <?php
                $cart = Session::get('cart');
                if (isset($cart) && !empty($cart)) {
                    $subTotal = 0;
                    foreach ($cart as $key => $value){
                        $subTotal += ($value['price'] * $value['qty']);
            ?>                                    
            <tr>
                <td class="cart_product">
                    <a href=""><img style="width: 100px; height: 100px; image-rendering: pixelated" src="http://training-laravel.com/upload/product/<?php echo $value['image'] ?>" alt=""></a>
                </td>
                <td class="cart_description">
                    <h4><a href="">{{$value['name']}}</a></h4>
                    <p>Web ID: 1089772</p>
                </td>
                <td class="cart_price" id="{{$value['price']}}">
                    <p>{{number_format($value['price'])}}</p>
                </td>
                <td class="cart_quantity">
                    <p>{{$value['qty']}}</p>
                </td>
                <td class="cart_total" id="{{$value['qty'] * $value['price']}}">
                    <p class="cart_total_price">{{number_format($value['qty'] * $value['price'])}}</p>
                </td>
            </tr>
            <?php
                    }
                } 
            ?>
        </tbody>
    </table>

</body>
</html>

