@extends('frontend.master.master')
@section('title')
	Check out | E-Shopper
@endsection
@section('content-cart')

<section id="cart_items">
    <div class="container">
        <div class="breadcrumbs">
            <ol class="breadcrumb">
              <li><a href="#">Home</a></li>
              <li class="active">Check out</li>
            </ol>
        </div><!--/breadcrums-->
            @if(session('success'))
            <div class="alert alert-success">
                {{session('success')}}
            </div>
            @endif
            @if(session('error'))
            <div class="alert alert-danger">
                {{session('error')}}
            </div>
            @endif
            <div class="signup-form"><!--sign up form-->
                <h2>New User Signup!</h2>
                <form method="post" action="{{ url('/register-checkout') }}">
                    @csrf
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif                   
                    <input type="text" name="name" placeholder="Name"/>
                    <input type="email" name="email" placeholder="Email"/>
                    <input type="password" name="password" placeholder="Password"/>
                    <input type="text" name="phone" placeholder="Phone"/>
                    <input type="text" name="address" placeholder="Address"/>
                    <button type="submit" class="btn btn-default">Signup</button>
                </form>
            </div><!--/sign up form-->
        <div class="review-payment">
            <h2>Review & Payment</h2>
        </div>

        <div class="table-responsive cart_info">
            <table class="table table-condensed">
                <thead>
                    <tr class="cart_menu">
                        <td class="image">Item</td>
                        <td class="description">Name</td>
                        <td class="price">Price</td>
                        <td class="quantity">Quantity</td>
                        <td class="total">Total</td>
                        <td>Action</td>
                    </tr>
                </thead>
                <tbody>
                <?php
                    $cart = Session::get('cart');
                    if (isset($cart) && !empty($cart)) {
                        $subTotal = 0;
                        foreach ($cart as $key => $value){
                            $subTotal += ($value['price'] * $value['qty']);
                ?>                                    
                <tr>
                    <td class="cart_product">
                        <a href=""><img style="width: 100px; height: 100px; image-rendering: pixelated" src="{{asset('upload/product/'.$value['image'])}}" alt=""></a>
                    </td>
                    <td class="cart_description">
                        <h4><a href="">{{$value['name']}}</a></h4>
                        <p>Web ID: 1089772</p>
                    </td>
                    <td class="cart_price" id="{{$value['price']}}">
                        <p>{{number_format($value['price'])}}</p>
                    </td>
                    <td class="cart_quantity">
                        <div class="cart_quantity_button">
                            <a style='cursor :pointer;' class="cart_quantity_up"  id="{{$value['id']}}"> + </a>
                            <input class="cart_quantity_input" id="qty_{{$value['id']}}" type="text" name="quantity" value="{{$value['qty']}}" autocomplete="off" size="2">
                            <a style='cursor :pointer;' class="cart_quantity_down"  id="{{$value['id']}}"> - </a>
                        </div>
                    </td>
                    <td class="cart_total" id="{{$value['qty'] * $value['price']}}">
                        <p class="cart_total_price">{{number_format($value['qty'] * $value['price'])}}</p>
                    </td>
                    <td class="cart_delete">
                        <a style='cursor :pointer;' class="cart_quantity_delete" id="{{$value['id']}}"><i class="fa fa-times"></i></a>
                    </td>
                </tr>
                <?php
                        }
                    } else {
                ?>
                    <tr>
                        <td colspan="5" id="notice">
                            <a style="color: red" href="{{url('/web')}}">Your Cart is empty ! please shopping now</a>
                        </td>
                    </tr>
                <?php        
                    }
                ?>
                    <tr>
                        <td colspan="4">&nbsp;</td>
                        <td colspan="2">
                            <table class="table table-condensed total-result">
                                <tr>
                                    <td>Cart Sub Total</td>
                                    <td><span id="subTotal" class="
                                        <?php 
                                            if (isset($subTotal)) {
                                               echo $subTotal;
                                            } else {
                                                echo 0;
                                            }          
                                        ?>">

                                        <?php 
                                            if (isset($subTotal)) {
                                               echo number_format($subTotal);
                                            } else {
                                                echo 0;
                                            }          
                                        ?>
                                        </span> 
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
        <div style="float: right; margin: 10px">
        <form method="post" id="abc" action="{{url('/order')}}">
            @csrf
            <button class="btn btn-info" type="submit">Oder Now</button>
        </form>
        </div>
        </div>
          
        <div class="payment-options">
            <span>
                <label><input type="checkbox"> Direct Bank Transfer</label>
            </span>
            <span>
                <label><input type="checkbox"> Check Payment</label>
            </span>
            <span>
                <label><input type="checkbox"> Paypal</label>
            </span>
        </div>
    </div>
</section> <!--/#cart_items-->


<script type="text/javascript">

        $('form#abc').submit(function(){
            var loggedIn = "{{Auth::check()}}";
            if ( loggedIn == "" ){ 
                alert('Please to login your account before order !');
                return false;
            }else {
                return true;
            }
        });

        //Update qty +
        $(".cart_quantity_up").click(function() {
            var id_up = $(this).attr('id');
            var abc = $(this);
            $.ajax({
                method: "POST",
                url: "{{url('/update-qty-up')}}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    id_up:id_up
                },
                success : function(up){
                    //console.log(up)
                    //format number in js
                    var getSub = up.subTotal.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
                    abc.next().attr('value',up.qty);
                    abc.closest('tr').find('.cart_total_price').text(up.totalPrice);
                    $("#subTotal").text(getSub);
                },          
            });

        });

        //Update qty -
        $(".cart_quantity_down").click(function() {
            var id_down = $(this).attr('id');
            var abc = $(this);
            $.ajax({
                method: "POST",
                url: "{{url('/update-qty-down')}}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    id_down:id_down
                },
                success : function(down){
                    //console.log(down)
                    //format number in js
                    var getSub = down.subTotal.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
                    if (down.qty < 0) {
                        abc.closest('tr').remove();
                        $('i.fa-shopping-cart').text(down.qtySession);
                    } else {
                        abc.prev().attr('value',down.qty);
                        abc.closest('tr').find('.cart_total_price').text(down.totalPrice);
                        $("#subTotal").text(getSub);
                    }                       
                }
            });

        });

        //delete cart X
        $(".cart_quantity_delete").click(function() {
            var id_delete = $(this).attr('id');
            var subTotal = $("#subTotal").attr('class');
            var totalPrice = $(this).closest('tr').find('.cart_total').attr('id');
            var abc = $(this);          
            //alert(sum)
            $.ajax({
                method: "POST",
                url: "{{url('/delete-cart')}}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    id_delete:id_delete
                    //subTotal:subTotal
                }, 
                success : function(data){
                    //console.log($(this));
                    console.log(subTotal); 
                    //var xxx = abc.closest('tr').find('.cart_total').attr('id'); 
                    var sum = parseInt(subTotal) - parseInt(totalPrice);
                    $("#subTotal").attr('class', sum);
                    var getSub = sum.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
                    $("#subTotal").text(getSub);
                    $('i.fa-shopping-cart').text(data.qty);
                    abc.closest('tr').remove();                  
                }
            });
        });
</script>
@endsection