@extends('frontend.master.master')
@section('title')
	Cart | E-Shopper
@endsection
@section('content-cart')

<section id="cart_items">
    <div class="container">
        <div class="breadcrumbs">
            <ol class="breadcrumb">
              <li><a href="#">Home</a></li>
              <li class="active">Shopping Cart</li>
            </ol>
        </div>
        <div class="table-responsive cart_info">
            <table class="table table-condensed">
                <thead>
                    <tr class="cart_menu">
                        <td class="image">Item</td>
                        <td class="description">Name</td>
                        <td class="price">Price</td>
                        <td class="quantity">Quantity</td>
                        <td class="total">Total</td>
                        <td>Action</td>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $cart = Session::get('cart');
                        if (isset($cart) && !empty($cart)) {
                            $subTotal = 0;
                            foreach ($cart as $key => $value){
                                $subTotal += ($value['price'] * $value['qty']);
                    ?>                                    
                    <tr>
                        <td class="cart_product">
                            <a href=""><img style="width: 100px; height: 100px; image-rendering: pixelated" src="{{asset('upload/product/'.$value['image'])}}" alt=""></a>
                        </td>
                        <td class="cart_description">
                            <h4><a href="">{{$value['name']}}</a></h4>
                            <p>Web ID: 1089772</p>
                        </td>
                        <td class="cart_price" id="{{$value['price']}}">
                            <p>{{number_format($value['price'])}}</p>
                        </td>
                        <td class="cart_quantity">
                            <div class="cart_quantity_button">
                                <a style='cursor :pointer;' class="cart_quantity_up"  id="{{$value['id']}}"> + </a>
                                <input class="cart_quantity_input" id="qty_{{$value['id']}}" type="text" name="quantity" value="{{$value['qty']}}" autocomplete="off" size="2">
                                <a style='cursor :pointer;' class="cart_quantity_down"  id="{{$value['id']}}"> - </a>
                            </div>
                        </td>
                        <td class="cart_total" id="{{$value['qty'] * $value['price']}}">
                            <p class="cart_total_price">{{number_format($value['qty'] * $value['price'])}}</p>
                        </td>
                        <td class="cart_delete">
                            <a style='cursor :pointer;' class="cart_quantity_delete" id="{{$value['id']}}"><i class="fa fa-times"></i></a>
                        </td>
                    </tr>
                    <?php
                            }
                        } else {
                    ?>
                        <tr>
                            <td colspan="5" id="notice">
                                <a style="color: red" href="{{url('/web')}}">Your Cart is empty ! please shopping now</a>
                            </td>
                        </tr>
                    <?php        
                        }
                    ?>

                </tbody>
            </table>
        </div>
    </div>
</section> <!--/#cart_items-->

<section id="do_action">
    <div class="container">
        <div class="heading">
            <h3>What would you like to do next?</h3>
            <p>Choose if you have a discount code or reward points you want to use or would like to estimate your delivery cost.</p>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="chose_area">
                    <ul class="user_option">
                        <li>
                            <input type="checkbox">
                            <label>Use Coupon Code</label>
                        </li>
                        <li>
                            <input type="checkbox">
                            <label>Use Gift Voucher</label>
                        </li>
                        <li>
                            <input type="checkbox">
                            <label>Estimate Shipping & Taxes</label>
                        </li>
                    </ul>
                    <ul class="user_info">
                        <li class="single_field">
                            <label>Country:</label>
                            <select>
                                <option>United States</option>
                                <option>Bangladesh</option>
                                <option>UK</option>
                                <option>India</option>
                                <option>Pakistan</option>
                                <option>Ucrane</option>
                                <option>Canada</option>
                                <option>Dubai</option>
                            </select>
                            
                        </li>
                        <li class="single_field">
                            <label>Region / State:</label>
                            <select>
                                <option>Select</option>
                                <option>Dhaka</option>
                                <option>London</option>
                                <option>Dillih</option>
                                <option>Lahore</option>
                                <option>Alaska</option>
                                <option>Canada</option>
                                <option>Dubai</option>
                            </select>
                        
                        </li>
                        <li class="single_field zip-field">
                            <label>Zip Code:</label>
                            <input type="text">
                        </li>
                    </ul>
                    <a class="btn btn-default update" href="">Get Quotes</a>
                    <a class="btn btn-default check_out" href="">Continue</a>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="total_area">
                    <ul>
                        <li>Cart Sub Total 
                            <span id="subTotal" class="
                            <?php 
                                if (isset($subTotal)) {
                                   echo $subTotal;
                                } else {
                                    echo 0;
                                }          
                            ?>">

                            <?php 
                                if (isset($subTotal)) {
                                   echo number_format($subTotal);
                                } else {
                                    echo 0;
                                }          
                            ?>              
                            </span>
                        </li>
                        <li>Eco Tax <span>$2</span></li>
                        <li>Shipping Cost <span>Free</span></li>
                        <li>Total <span>$61</span></li>
                    </ul>
                        <a class="btn btn-default update" href="">Update</a>
                        <a class="btn btn-default check_out" href="{{url('/checkout')}}">Check Out</a>
                </div>
            </div>
        </div>
    </div>
</section><!--/#do_action-->

<script type="text/javascript">
        //Update qty +
        $(".cart_quantity_up").click(function() {
            var id_up = $(this).attr('id');
            var abc = $(this);
            $.ajax({
                method: "POST",
                url: "{{url('/update-qty-up')}}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    id_up:id_up
                },
                success : function(up){
                    //console.log(up)
                    //format number in js
                    var getSub = up.subTotal.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
                    abc.next().attr('value',up.qty);
                    abc.closest('tr').find('.cart_total_price').text(up.totalPrice);
                    $("#subTotal").text(getSub);
                },          
            });

        });

        //Update qty -
        $(".cart_quantity_down").click(function() {
            var id_down = $(this).attr('id');
            var abc = $(this);
            $.ajax({
                method: "POST",
                url: "{{url('/update-qty-down')}}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    id_down:id_down
                },
                success : function(down){
                    //console.log(down)
                    //format number in js
                    var getSub = down.subTotal.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
                    if (down.qty < 0) {
                        abc.closest('tr').remove();
                        $('i.fa-shopping-cart').text(down.qtySession);
                    } else {
                        abc.prev().attr('value',down.qty);
                        abc.closest('tr').find('.cart_total_price').text(down.totalPrice);
                        $("#subTotal").text(getSub);
                    }                       
                }
            });

        });

        //delete cart X
        $(".cart_quantity_delete").click(function() {
            var id_delete = $(this).attr('id');
            var subTotal = $("#subTotal").attr('class');
            var totalPrice = $(this).closest('tr').find('.cart_total').attr('id');
            var abc = $(this);          
            //alert(sum)
            $.ajax({
                method: "POST",
                url: "{{url('/delete-cart')}}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    id_delete:id_delete
                }, 
                success : function(data){
                    //console.log($(this));
                    console.log(subTotal); 
                    //var xxx = abc.closest('tr').find('.cart_total').attr('id'); 
                    var sum = parseInt(subTotal) - parseInt(totalPrice);
                    $("#subTotal").attr('class', sum);
                    var getSub = sum.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
                    $("#subTotal").text(getSub);
                    $('i.fa-shopping-cart').text(data);
                    abc.closest('tr').remove();                  
                }
            });
        });
</script>
@endsection