<div class="left-sidebar">
	<h2>Category</h2>
	<div class="panel-group category-products" id="accordian"><!--category-productsr-->
		@foreach($category as $categories)
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="panel-title">
					<a href="{{url('/category/'.$categories->id)}}">
						<span class="badge pull-right"><i class="fa fa-plus"></i></span>
						{{$categories->category_name}}
					</a>
				</h4>
			</div>
		</div>
		@endforeach
	</div><!--/category-products-->

	<div class="brands_products"><!--brands_products-->
		<h2>Brands</h2>
		<div class="brands-name">
			<ul class="nav nav-pills nav-stacked">
				@foreach($brand as $brands)
				<li><a href="{{url('/brand/'.$brands->id)}}"> <span class="pull-right">(50)</span>{{$brands->brand_name}}</a></li>
				@endforeach
			</ul>
		</div>
	</div><!--/brands_products-->
	
	<div class="price-range"><!--price-range-->
		<h2>Price Range</h2>
		<div class="well text-center">
			 <input type="text" class="span2" value="" data-slider-min="0" data-slider-max="60000" data-slider-step="5" data-slider-value="[25000,45000]" id="sl2" ><br />
			 <b class="pull-left">$ 0</b> <b class="pull-right">$ 60000</b>
		</div>
	</div><!--/price-range-->
	
	<div class="shipping text-center"><!--shipping-->
		<img src="{{asset('frontend/images/home/shipping.jpg')}}" alt="" />
	</div><!--/shipping-->

</div>

<script>
	$(document).ready(function(){
		$('.text-center').click(function() {
			var getPrice = $('.span2').data('value');
			var jsonPrice = JSON.stringify(getPrice);
			 console.log(getPrice)
			// console.log(getPrice[0])
			// console.log(getPrice[1])

			$.ajax ({
	        	url:"{{url('/search-ajax')}}",
	        	method: "POST",
	        	data: {
	        		"_token": "{{ csrf_token() }}",
	        		jsonPrice:jsonPrice
	        	},
	        	success:function(data) {

	        		if (data) {
	        			var tbHtml = '';
	        			Object.keys(data).map((key, value) => {
							//console.log(data[key]);

							tbHtml += "<tr class='tr_"+key+"' id='container'>" +
											// "<td class='cart_product'>" +
											// 	"<a><img src= "+data[key]['img']+ "></a>" +
											// "</td>"+

											"<td class='cart_description'>"+
												"<h4><a href=''>"+data[key]['description']+"</a></h4>"+
												"<p>Web ID: 1089772</p>"+
											"</td>"+

											"<td class='cart_price'>"+
												"<p>"+data[key]['price']+"</p>"+
											"</td>"+

											"<td class='cart_total'>"+
											"<p class='cart_total_price' id ='total_"+key+"'>$"+ "<span>" + data[key]['qty'] * data[key]['price'] + "</span>" +"</p>"+
											"</td>"+

										"</tr>";
						})

						console.log(tbHtml);
						//$('#search-ajax').hide();
	        			$('#search-ajax').html(tbHtml)
	        		}
	        	}
	        });

		});
	});
</script>
