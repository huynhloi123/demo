<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>@yield('title')</title>
    <link href="{{asset('frontend/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('frontend/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('frontend/css/prettyPhoto.css')}}" rel="stylesheet">
    <link href="{{asset('frontend/css/price-range.css')}}" rel="stylesheet">
    <link href="{{asset('frontend/css/animate.css')}}" rel="stylesheet">
	<link href="{{asset('frontend/css/main.css')}}" rel="stylesheet">
	<link href="{{asset('frontend/css/responsive.css')}}" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="{{asset('frontend/images/ico/favicon.ico')}}">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{asset('frontend/images/ico/apple-touch-icon-144-precomposed.png')}}">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{asset('frontend/images/ico/apple-touch-icon-114-precomposed.png')}}">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{asset('frontend/images/ico/apple-touch-icon-72-precomposed.png')}}">
    <link rel="apple-touch-icon-precomposed" href="{{asset('frontend/images/ico/apple-touch-icon-57-precomposed.png')}}">

    <script src="{{asset('frontend/js/jquery.js')}}"></script>
	<script src="{{asset('frontend/js/bootstrap.min.js')}}"></script>
	<script src="{{asset('frontend/js/jquery.scrollUp.min.js')}}"></script>

</head><!--/head-->

<body>

	@include('frontend.master.topbar')
	
	@if ($_SERVER['REQUEST_URI'] == '/web')
	   @include('frontend.master.slider')
	@endif

	
	<section>
		<div class="container">
			<div class="row">
				@yield('content-cart')
				<div class="col-sm-3">
					@if($_SERVER['REQUEST_URI'] == '/')
						@include('frontend.master.leftsidebar')
					@endif
					
					<?php
						$array = array('web', "blog", 'details', 'search' , 'category', 'brand');
						$path = $_SERVER['REQUEST_URI'];

						foreach ($array as $key => $value) {
							//var_dump(strpos($path, $value));
							if (strpos($path, $value) !== false) {
					?>
						@include('frontend.master.leftsidebar')
					<?php
														
							}
						}
					?>

					<?php
						$array = array('account', 'product');
						$path = $_SERVER['REQUEST_URI'];

						foreach ($array as $key => $value) {
							//var_dump(strpos($path, $value));
							if (strpos($path, $value) !== false) {
					?>
						@include('frontend.master.leftsidebar1')
					<?php
														
							}
						}
					?>
				</div>
				
				<div class="col-sm-9 padding-right">
					@yield('content')
				</div>
			</div>
		</div>
	</section>

	@include('frontend.master.footer')

	<script src="{{asset('frontend/js/price-range.js')}}"></script>
    <script src="{{asset('frontend/js/jquery.prettyPhoto.js')}}"></script>
    <script src="{{asset('frontend/js/main.js')}}"></script>

	

</body>
</html>
					{{-- $explode =  explode("/", $_SERVER['REQUEST_URI']);
						var_dump($explode); --}}


						{{-- 				<?php
											if ($_SERVER['REQUEST_URI']) {
												echo $_SERVER['REQUEST_URI'];
										?>

										<?php
											}
										?>
						 --}}

						{{-- 				<?php
											if ($_SERVER['REQUEST_URI'] == '/blog') {
										?>
												@include('frontend.master.leftsidebar');
										<?php
											}
										?>
										@if ($_SERVER['REQUEST_URI'] == '/web')

										    @include('frontend.master.leftsidebar')
										@endif
							
										@if ($_SERVER['REQUEST_URI'] == '/blogSingle')

										    @include('frontend.master.leftsidebar')
										@endif --}}
