@extends('frontend.master.master')
@section('title')
	Register | E-Shopper
@endsection
@section('content')

<div class="container">
    <div class="row">
        <div class="col-sm-4">
        	<div class="signup-form"><!--sign up form-->
        		<h2>New User Signup!</h2>
        		<form method="post" action="{{ url('/member/insert-register') }}" enctype="multipart/form-data">
        			@csrf
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    @if(session('success'))
                    <div class="alert alert-success">
                        {{session('success')}}
                    </div>
                    @endif

                    @if(session('error'))
                    <div class="alert alert-danger">
                        {{session('error')}}
                    </div>
                    @endif
                    
        			<input type="text" name="name" placeholder="Name"/>
        			<input type="email" name="email" placeholder="Email"/>
        			<input type="password" name="password" placeholder="Password"/>
        			<input type="text" name="phone" placeholder="Phone"/>
        			<input type="text" name="address" placeholder="Address"/>
        			<input type="file" name="avatar"/>
        			<select name="country_id">
        				<option value="0">-----Choose Country-----</option>
        				@foreach($country as $countries)                              
                            <option value="{{ $countries->id }}">{{$countries->country_name}}</option>
                        @endforeach
        			</select>
        			<button type="submit" class="btn btn-default">Signup</button>
        		</form>
        	</div><!--/sign up form-->
        </div>
    </div>
</div>

@endsection
