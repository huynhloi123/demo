@extends('frontend.master.master')
@section('title')
	Account | E-Shopper
@endsection
@section('content')

    <div class="container">
        <div class="row">
			<div class="col-sm-4 col-sm-offset-1">
				<h2 class="title text-center">Update Member</h2>	
				<div class="login-form"><!--login form-->
					
					<form method="post" action="{{ url('/member/update-member') }}" enctype="multipart/form-data">
						@csrf
						@if ($errors->any())
			                <div class="alert alert-danger">
			                    <ul>
			                        @foreach($errors->all() as $error)
			                        <li>{{ $error }}</li>
			                        @endforeach
			                    </ul>
			                </div>
			            @endif

			            @if(session('success'))
			            <div class="alert alert-success">
			                {{session('success')}}
			            </div>
			            @endif

			            @if(session('error'))
			            <div class="alert alert-danger">
			                {{session('error')}}
			            </div>
			            @endif
			            <input type="text" name="name" placeholder="Name" value="{{$user->name}}">
						<input type="email" readonly="" name="email" placeholder="Email" value="{{$user->email}}" />
						<input type="password" name="password" value="" />
						<input type="text" name="address" placeholder="Address" value="{{$user->address}}" >

                        <select  name="country_id" required style="margin-bottom: 10px; padding: 10px" >
                        <option value="0">-----Choose Country-----</option>
                        
							@foreach($country as $countries)
								@if($user->country_id == $countries->id)
	                                <option selected value="{{ $countries->id }}">{{$countries->country_name}}</option>
	                                @else 
	                                    <option value="{{ $countries->id }}">{{$countries->country_name}}</option>
	                            @endif
	                        @endforeach

                    	</select>
						<input type="text" name="phone" placeholder="Phone" value="{{$user->phone}}">
						<img src="{{asset('upload/user/avatar/'.$user->avatar)}}" style="height: 100px; image-rendering: pixelated">
						<input type="file" name="avatar">
						<button type="submit" class="btn btn-default">Update</button>
					</form>
				</div><!--/login form-->
			</div>
        </div>
    </div>

@endsection