@extends('frontend.master.master')
@section('title')
	Login | E-Shopper
@endsection
@section('content')

<div class="container">
    <div class="row">
        <div class="col-sm-4 col-sm-offset-1">
        	<div class="login-form"><!--login form-->
        		<h2>Login to your account</h2>
        		<form method="post" action="{{ url('/member/signin') }}">
        			@csrf

                    @if(session('success'))
                    <div class="alert alert-success">
                        {{session('success')}}
                    </div>
                    @endif

                    @if(session('error'))
                    <div class="alert alert-danger">
                        {{session('error')}}
                    </div>
                    @endif

        			<input type="email" name="email" placeholder="Email" />
                    @if($errors->has('email'))
                        {{$errors->first('email')}}
                    @endif

        			<input type="password" name="password" placeholder="Password" />
                            {{-- 			<span>
                            				<input type="checkbox" class="checkbox"> 
                            				Keep me signed in
                            			</span> --}}
                    @if($errors->has('password'))
                        {{$errors->first('password')}}
                    @endif
        			<button type="submit" class="btn btn-default">Login</button>
        		</form>

        	</div><!--/login form-->
        </div>
    </div>
</div>

@endsection